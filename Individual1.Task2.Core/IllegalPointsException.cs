﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task2.Core
{
    public class IllegalPointsException : ArgumentException
    {
        public IllegalPointsException(string message) : base(message)
        { }

        public IllegalPointsException(string message, Exception innerException) : base(message, innerException)
        { }

    }
}
