﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task2.Core
{
    public class Rectangle
    {
        private Point[] _points;

        // You can put 2 or 4 points
        public Rectangle(params Point[] points)
        {
            if (points.Length != 2 && points.Length != 4)
            {
                throw new IllegalPointsException("Incorrect count of points");
            }

            if (points.Length == 2)
            {
                _points = GenerateByTwoPoints(points);
            }
            else
            {
                if (!AreLinesParallel(points))
                {
                    throw new IllegalPointsException("Lines of rectangle aren't parallel OX or OY");
                }

                _points = points;
            }
        }

        private Point[] GenerateByTwoPoints(Point[] points)
        {
            var newPoints = new Point[4] 
            {
                points[0],
                new Point (points[0].X, points[1].Y),
                points[1],
                new Point (points[1].X, points[0].Y)
            };

            return newPoints;
        }

        private bool AreLinesParallel(Point[] points)
        {
            return IsLineParallel(points[0], points[1]) 
                && IsLineParallel(points[1], points[2]) 
                && IsLineParallel(points[2], points[3]) 
                && IsLineParallel(points[3], points[0]);
        }

        private bool IsLineParallel(Point startPoint, Point endPoint)
        {
            return startPoint.X == endPoint.X || startPoint.Y == endPoint.Y;
        }

        private void AssertCorrectIndex(int index)
        {
            if (index < 0 || index > 3)
            {
                throw new IndexOutOfRangeException("Incorrect index");
            }
        }

        public Point this[int index]
        {
            get
            {
                AssertCorrectIndex(index);

                return _points[index];
            }

            set
            {
                AssertCorrectIndex(index);

                _points[index] = value;
            }
        }

        public double Height
        {
            get
            {
                return MathHelper.GetLength(_points[0], _points[1]);
            }
        }

        public double Width
        {
            get
            {
                return MathHelper.GetLength(_points[1], _points[2]);
            }
        }

        public void IncreaseHeight(double value)
        {
            _points[0] = new Point(_points[0].X, _points[0].Y - value);
            _points[1] = new Point(_points[1].X, _points[1].Y - value);
        }

        public void IncreaseWidth(double value)
        {
            _points[0] = new Point(_points[0].X - value, _points[0].Y);
            _points[1] = new Point(_points[1].X - value, _points[1].Y);
        }

        public void Move(MoveDirection direction, double value)
        {
            if (direction == MoveDirection.Top)
            {
                for (int i = 0; i <= 3; i++)
                {
                    _points[i] = new Point(_points[i].X, _points[i].Y + value);
                }
            }
            else if (direction == MoveDirection.Right)
            {
                for (int i = 0; i <= 3; i++)
                {
                    _points[i] = new Point(_points[i].X + value, _points[i].Y);
                }
            }
            else if (direction == MoveDirection.Bottom)
            {
                for (int i = 0; i <= 3; i++)
                {
                    _points[i] = new Point(_points[i].X, _points[i].Y - value);
                }
            }
            else if (direction == MoveDirection.Left)
            {
                for (int i = 0; i <= 3; i++)
                {
                    _points[i] = new Point(_points[i].X - value, _points[i].Y);
                }
            }
        }

        public static Rectangle Union(Rectangle r1, Rectangle r2)
        {
            double minX = Math.Min(r1[0].X, r2[0].X);
            double minY = Math.Min(r1[0].Y, r2[0].Y);

            double maxX = Math.Max(r1[2].X, r2[2].X);
            double maxY = Math.Max(r1[2].Y, r2[2].Y);

            Point p1 = new Point(minX, minY);
            Point p2 = new Point(maxX, maxY);

            return new Rectangle(p1, p2);
        }

        public static Rectangle Intersect(Rectangle r1, Rectangle r2)
        {
            Point a = new Point
            (
                Math.Min(r1[2].X, r2[2].X),
                Math.Min(r1[2].Y, r2[2].Y)
            );

            Point b = new Point
            (
                Math.Max(r1[0].X, r2[0].X),
                Math.Max(r1[0].Y, r2[0].Y)
            );

            if (a.X < b.X && a.Y > b.Y)
            {
                throw new InvalidOperationException("Rectangles haven't common points");
            }

            return new Rectangle(a, b);
        }

        public override string ToString()
        {
            var text = "";

            foreach (var point in _points)
            {
                text += point + " ";
            }

            return text;
        }
    }
}
