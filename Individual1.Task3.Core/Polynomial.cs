﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task3.Core
{
    public class Polynomial : IEnumerable<PolynomialMember>
    {
        private List<PolynomialMember> _members;

        public Polynomial()
        {
            _members = new List<PolynomialMember>();
        }

        public Polynomial(params PolynomialMember[] members) : this()
        {
            AddMembers(members);
        }

        public Polynomial(double[] coefficients, int[] powers) : this()
        {
            if (coefficients.Length != powers.Length)
            {
                throw new ArgumentException("Length of coefficients and powers should be equals");
            }

            PolynomialMember[] members = new PolynomialMember[coefficients.Length];

            for (int i = 0; i < coefficients.Length; i++)
            {
                members[i] = new PolynomialMember(coefficients[i], powers[i]);
            }

            AddMembers(members);
        }

        public void AddMember(PolynomialMember member)
        {
            _members.Add(member);
        }

        public void AddMembers(IEnumerable<PolynomialMember> members)
        {
            _members.AddRange(members);
        }

        public bool RemoveMember(PolynomialMember member)
        {
            return _members.Remove(member);
        }

        public double Calculate(double x)
        {
            double sum = 0;

            _members.ForEach(m => sum += m.Calculate(x));

            return sum;
        }

        public IEnumerator<PolynomialMember> GetEnumerator()
        {
            return _members.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static Polynomial operator+(Polynomial p1, Polynomial p2)
        {
            var newPolynomial = new Polynomial();
            newPolynomial.AddMembers(p1);
            newPolynomial.AddMembers(p2);

            return newPolynomial;
        }

        public static Polynomial operator-(Polynomial p1, Polynomial p2)
        {
            var newPolynomial = new Polynomial();
            newPolynomial.AddMembers(p1);
            
            foreach (PolynomialMember member in p2)
            {
                newPolynomial.AddMember(new PolynomialMember(member.Coefficient * -1, member.Power));
            }

            return newPolynomial;
        }

        public static Polynomial operator*(Polynomial p1, Polynomial p2)
        {
            var newPolynomial = new Polynomial();

            foreach (PolynomialMember memberOuter in p1)
            {
                foreach(PolynomialMember memberInner in p2)
                {
                    newPolynomial.AddMember(new PolynomialMember
                    (
                        memberOuter.Coefficient * memberInner.Coefficient,
                        memberOuter.Power + memberInner.Power
                    ));
                }
            }

            return newPolynomial;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < _members.Count; i++)
            {
                if (i != 0 && _members[i].Coefficient >= 0)
                {
                    builder.Append("+");
                }

                builder.AppendFormat("{0}x^{1}", _members[i].Coefficient, _members[i].Power);
            }

            return builder.ToString();
        }

    }
}
