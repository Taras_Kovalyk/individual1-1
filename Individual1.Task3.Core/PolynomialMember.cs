﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task3.Core
{
    public class PolynomialMember
    {
        public PolynomialMember() : this(1, 1)
        {
        }

        public PolynomialMember(double cofficient, int power)
        {
            Coefficient = cofficient;
            Power = power;
        }

        public double Coefficient
        {
            get;
            set;
        }

        public int Power
        {
            get;
            set;
        }

        public double Calculate(double x)
        {
            return Math.Pow(Coefficient * x, Power);
        }
    }
}
